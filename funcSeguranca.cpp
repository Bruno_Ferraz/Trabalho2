#include "pessoa.hpp"
#include "funcSeguranca.hpp"
#include <string>


FuncSeguranca::FuncSeguranca(string nome, string sobrenome, long id_usuario, long id_disciplina, string fotos){
	setPessoa(Pessoa(nome, sobrenome, id_usuario, id_disciplina, fotos));
}

void FuncSeguranca::setPessoa(Pessoa P){
	setNome(P.getNome());
	setSobrenome(P.getSobrenome());  
	setId_usuario(P.getId_usuario());
	setId_disciplina(P.getId_disciplina());
	setFotos(P.getFotos());
}

