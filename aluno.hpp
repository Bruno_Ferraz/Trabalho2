#ifndef _ALUNO_HPP
#define _ALUNO_HPP

#include "pessoa.hpp"
#include<string>

using namespace std;

class Aluno:public Pessoa{

public:
	Aluno(){};
	Aluno(string nome, string sobrenome, long id_usuario, long id_disciplina, string fotos);
	void setPessoa(Pessoa P);
};

#endif
