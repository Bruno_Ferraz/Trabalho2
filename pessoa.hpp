#ifndef _PESSOA_HPP
#define _PESSOA_HPP

#include <string>

using namespace std;

class Pessoa{
  
public:
	string nome;
	string sobrenome;	
	long id_usuario;
	string fotos;
	long id_disciplina;
	Pessoa();
	Pessoa(string nome, string sobrenome, long id_usuario, long id_disciplina, string fotos);
	long getId_usuario();
	void setId_usuario(long id_usuario);
	string getNome();
	void setNome(string nome);
	string getSobrenome();
	void setSobrenome(string sobrenome);
	long getId_disciplina();
	void setId_disciplina(long id_disciplina);
	string getFotos();
	void setFotos(string fotos);
};


#endif
