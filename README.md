# Introdução


Este sistema tem como objetivo auxiliar o controle de acesso à um grupo de salas, fornecendo diversas ferramentas para a realização deste fim, dentre elas, ferramenta de cadastro, remoção e consulta de usuários e reservas. Sendo a autenticação realizada através de reconhecimento facial e o armazenamento das informações relacionadas à cada usuário e cada reserva são armazenados em disco. O usuário que será cadastrado, deverá informar ao administrador do sistema seu nome, sobrenome, id de usuário, id de disciplina e o repositório das fotos padrão, que serão usadas para a autenticação, após a realização do cadastro, as informações serão gravadas em memória, e somente serão aplicadas em disco ao encerrar o programa corretamente, esta condição também é válida para a remoção de usuários e reservas. A ferramenta de consulta tem como objetivo principal realizar a autenticação de usuários, pois ao ser executada, ela buscará o id de usuário no banco de dados e caso encontre o id no banco, irá comparar as fotos presentes no diretório informado no cadastro com as fotos tiradas no momento da tentativa de acesso. O sistema também fornece a possibilidade de limpar o banco de dados, limpando todos os dados cadastrados, tanto de reserva quanto de usuários.




# Requisitos e instalação


Para a execução do programa, é necessária a realização da compilação do código, que preferencialmente pode ser realizada através do Cmake, e a sua instalação pode ser feita seguindo o tutorial da página: https://cmake.org/install/  . Após a instalação do Cmake, deverá ser criado o Makefile, para isso, deve-se acessar o Termonal, entrar no diretório do sistema e utilizar o comando "cmake ./", com isso o arquivo Makefile será criado, e o programa poderá ser compilado, através do comando "make", criando assim o arquivo executável "Trabalho". Finalmente, para a execução do sistema, deverá ser digitado o comando "./Trabalho" e o programa se iniciará.




# Instruções de uso


Após a inicialização do programa, será exibido o seguinte menu:


![alt text] (Prints/Menu.jpeg)





Escolha a opção desejada, caso esta opção seja cadastro de usuário, digite a opção 1 e dê enter, será exibido um menu para a escolha do tipo de cadastro, digite os dados solicitados e ao final, será retornado ao menu de cadastro, como na foto abaixo:


![alt text] (Prints/print_cadastro_usuario.jpeg)






Caso a opção desejada seja cadastrar uma reserva, siga os passos acima até a seleção do tipo de cadastro, porém digite a opção 7, como na foto abaixo:


![alt text] (Prints/print_cadastro_reserva.jpeg)






Se a opção desejada for a remoção de usuário, digite a opção 2 e dê enter, será exibido um menu para a escolha do tipo de remoção, digite os dados solicitados e ao final, será retornado ao menu de remoção, como na foto abaixo:


![alt text] (Prints/print_remocao_usuario.jpeg)






Caso a opção desejada seja remover uma reserva, siga os passos acima até a seleção do tipo de remoção, porém digite a opção 7, como na foto abaixo:


![alt text] (Prints/print_remocao_reserva.jpeg)





Se a opção desejada for a consulta de usuário, digite a opção 3 e dê enter, será exibido um menu para a escolha do tipo de consulta, digite os dados solicitados e ao final, será retornado ao menu de consulta, como na foto abaixo:


![alt text] (Prints/print_consulta_usuario.jpeg)





Caso a opção desejada seja consultar uma reserva, siga os passos acima até a seleção do tipo de consulta, porém digite a opção 7, como na foto abaixo:


![alt text] (Prints/print_consulta_reserva.jpeg)





Se a opção desejada for a reinicialização do banco de dados, digite a opção 4 e dê enter, com isso será retornado ao menu inicial.


Para uma melhor visualização do comportamento do programa, seguem três diagramas de sequência, com o objetivo de mostrar o funcionamento do programa de acordo com as opções escolhidas, no caso, cadastro, consulta e remoção.


![alt text] (Diagramas/diagrama_cadastro.jpeg)





![alt text] (Diagramas/diagrama_consulta.jpeg)




![alt text] (Diagramas/diagrama_remocao.jpeg)







