#ifndef _FUNC_LIMPEZA_HPP
#define _FUNC_LIMPEZA_HPP

#include "pessoa.hpp"
#include<string>

using namespace std;

class FuncLimpeza:public Pessoa{

public:
	FuncLimpeza(){};
	FuncLimpeza(string nome, string sobrenome, long id_usuario, long id_disciplina, string fotos);
	void setPessoa(Pessoa P);
};


#endif
