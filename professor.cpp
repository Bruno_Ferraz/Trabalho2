#include "pessoa.hpp"
#include "professor.hpp"
#include <string>

Professor::Professor(string nome, string sobrenome, long id_usuario, long id_disciplina, string fotos){
	setPessoa(Pessoa(nome, sobrenome, id_usuario, id_disciplina, fotos));
}

void Professor::setPessoa(Pessoa P){
	setNome(P.getNome());
	setSobrenome(P.getSobrenome());  
	setId_usuario(P.getId_usuario());
	setId_disciplina(P.getId_disciplina());
	setFotos(P.getFotos());
}

