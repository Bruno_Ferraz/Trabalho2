#ifndef _SUPORTE_HPP
#define _SUPORTE_HPP

#include "pessoa.hpp"
#include <string>

using namespace std;

class Suporte: public Pessoa{

public:
	Suporte(){};
	Suporte(string nome, string sobrenome, long id_usuario, long id_disciplina, string fotos);
	void setPessoa(Pessoa P);
};

#endif
