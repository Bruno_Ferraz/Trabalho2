#ifndef _EXTERNOS_HPP
#define _EXTERNOS_HPP

#include "pessoa.hpp"
#include<string>

using namespace std;

class Externos:public Pessoa{

public:
	Externos(){};
	Externos(string nome, string sobrenome, long id_usuario, long id_disciplina, string fotos);
	void setPessoa(Pessoa P);
};

#endif
