#include "pessoa.hpp"
#include "aluno.hpp"
#include <string>

Aluno::Aluno(string nome, string sobrenome, long id_usuario, long id_disciplina, string fotos){
	this->nome = nome;
	this->sobrenome = sobrenome;
	this->id_usuario = id_usuario;
	this->id_disciplina = id_disciplina;
	this->fotos = fotos;
}

void Aluno::setPessoa(Pessoa P){
	setNome(P.getNome());
	setSobrenome(P.getSobrenome());  
	setId_disciplina(P.getId_disciplina());
	setId_usuario(P.getId_usuario());
	setFotos(P.getFotos());
}
