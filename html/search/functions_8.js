var searchData=
[
  ['saida',['saida',['../main_8cpp.html#a46851a69b6140500d8f59ad9b1683b01',1,'main.cpp']]],
  ['setfotos',['setFotos',['../classPessoa.html#a9809a7d2c242c9f1b0e88db032d97cf2',1,'Pessoa']]],
  ['setid_5fdisciplina',['setId_disciplina',['../classPessoa.html#a0a30076675053373e9cf5b710a0ab15c',1,'Pessoa']]],
  ['setid_5fusuario',['setId_usuario',['../classPessoa.html#ada36e7e3f17c6dc3dee95edfaceb3b6e',1,'Pessoa']]],
  ['setnome',['setNome',['../classPessoa.html#ab059a835c4aa05bd77dcc58297fca331',1,'Pessoa']]],
  ['setpessoa',['setPessoa',['../classAluno.html#a550a98f7d447e63fd693c900e5ada0c1',1,'Aluno::setPessoa()'],['../classExternos.html#aa83bfeff0318cc4c693354dd5b517b96',1,'Externos::setPessoa()'],['../classFuncLimpeza.html#a85bc06b1a35cce976423ea60615fe189',1,'FuncLimpeza::setPessoa()'],['../classFuncSeguranca.html#ac8cda100e611c2a3ee3d858e76375ef1',1,'FuncSeguranca::setPessoa()'],['../classProfessor.html#a35c6798fe61b36aad8d8b4b5d75255d2',1,'Professor::setPessoa()'],['../classSuporte.html#a0c723972f69b62077d5aaf40de740cda',1,'Suporte::setPessoa()']]],
  ['setsobrenome',['setSobrenome',['../classPessoa.html#aa4a700a1f6028471d9cb3f3dd23bfc46',1,'Pessoa']]],
  ['suporte',['Suporte',['../classSuporte.html#a7bee507235933aa0fb0f96a4d69110e5',1,'Suporte::Suporte()'],['../classSuporte.html#a6e7b709a7f04f77b96c191e6983fdf6b',1,'Suporte::Suporte(string nome, string sobrenome, long id_usuario, long id_disciplina, string fotos)']]]
];
