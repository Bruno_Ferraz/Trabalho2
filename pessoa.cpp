#include "pessoa.hpp"
#include<string>

Pessoa::Pessoa(){
	nome = "";
	sobrenome = "";
	id_usuario = 0;
	id_disciplina = 0;
	fotos = "";
}
Pessoa::Pessoa(string nome, string sobrenome, long id_usuario, long id_disciplina, string fotos){
	setNome(nome);
	setSobrenome(sobrenome);
	setId_usuario(id_usuario);
	setFotos(fotos);
	setId_disciplina(id_disciplina);
}
string Pessoa::getNome() {
  return nome;
}
void Pessoa::setNome(string nome) {
  this->nome = nome;
}

string Pessoa::getSobrenome() {
  return sobrenome;
}
void Pessoa::setSobrenome(string sobrenome) {
  this->sobrenome = sobrenome;
}

long Pessoa::getId_usuario() {
  return id_usuario;
}
void Pessoa::setId_usuario(long id_usuario)
{
  this->id_usuario = id_usuario;
}

string Pessoa::getFotos() {
  return fotos;
}

void Pessoa::setFotos(string fotos) {
  this->fotos = fotos;
}
long Pessoa::getId_disciplina() {
  return id_disciplina;
}
void Pessoa::setId_disciplina(long id_disciplina) {
  this->id_disciplina = id_disciplina;
}


