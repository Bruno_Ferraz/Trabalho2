#ifndef _FUNC_SEGURANCA_HPP
#define _FUNC_SEGURANCA_HPP

#include "pessoa.hpp"
#include<string>

using namespace std;

class FuncSeguranca:public Pessoa{

public:
	FuncSeguranca(){};
	FuncSeguranca(string nome, string sobrenome, long id_usuario, long id_disciplina, string fotos);
	void setPessoa(Pessoa P);
};

#endif
