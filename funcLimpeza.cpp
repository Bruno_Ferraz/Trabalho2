#include "pessoa.hpp"
#include "funcLimpeza.hpp"
#include <string>


FuncLimpeza::FuncLimpeza(string nome, string sobrenome, long id_usuario, long id_disciplina, string fotos){
	setPessoa(Pessoa(nome, sobrenome, id_usuario, id_disciplina, fotos));
}

void FuncLimpeza::setPessoa(Pessoa P){
	setNome(P.getNome());
	setSobrenome(P.getSobrenome());  
	setId_usuario(P.getId_usuario());
	setId_disciplina(P.getId_disciplina());
	setFotos(P.getFotos());
}

