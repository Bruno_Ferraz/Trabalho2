#include "pessoa.hpp"
#include "pessoa.cpp"
#include "aluno.cpp"
#include "aluno.hpp"
#include "professor.cpp"
#include "professor.hpp"
#include "funcLimpeza.cpp"
#include "funcLimpeza.hpp"
#include "funcSeguranca.cpp"
#include "funcSeguranca.hpp"
#include "externos.cpp"
#include "externos.hpp"
#include "suporte.cpp"
#include "suporte.hpp"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>


using namespace std;


struct Ids{			//Estrutura que servirá para armazenar os ids fornecidos na criação de uma reserva.
	long ids[30];
};

struct Sala {						//Estrutura que representa cada sala, contendo uma matriz de Ids, com o objetivo de representar os meses 
	Ids horario[30][24];	// e horas das reservas.
};


Sala Salas[10];								//Variáveis globais que serão gravadas e lidas em disco.
Pessoa P;
Aluno Alunos[20];
Professor Professores[20];
FuncLimpeza Limpeza[20];
Suporte Suporte[20];
Externos Externos[20];
FuncSeguranca Segurancas[20];
int numero_alunos,numero_professores,numero_segurancas,numero_limpeza,numero_externos,numero_suporte,numeros[6];

void entrada(){		//Função responsável por abrir o arquivo "BANCO.txt", e inserir os dados presentes nas variáveis globais.

	int i,j,k,x;
	
	ifstream in; 		
	in.open("BANCO.txt"); 			//Abertura do arquivo.
	in>>numero_alunos;					//Inicio da leitura das variáveis
	in>>numero_professores;
	in>>numero_segurancas;
	in>>numero_limpeza;
	in>>numero_externos;
	in>>numero_suporte;
	for(i=0;i<20;i++){					//Loop responsável por inicializar cada instância de usuário.
		in>>Alunos[i].nome;
		in>>Alunos[i].sobrenome;
		in>>Alunos[i].id_usuario;
		in>>Alunos[i].id_disciplina;
		in>>Alunos[i].fotos;
		in>>Professores[i].nome;
		in>>Professores[i].sobrenome;
		in>>Professores[i].id_usuario;
		in>>Professores[i].id_disciplina;
		in>>Professores[i].fotos;
		in>>Segurancas[i].nome;
		in>>Segurancas[i].sobrenome;
		in>>Segurancas[i].id_usuario;
		in>>Segurancas[i].id_disciplina;
		in>>Segurancas[i].fotos;
		in>>Suporte[i].nome;
		in>>Suporte[i].sobrenome;
		in>>Suporte[i].id_usuario;
		in>>Suporte[i].id_disciplina;
		in>>Suporte[i].fotos;
		in>>Limpeza[i].nome;
		in>>Limpeza[i].sobrenome;
		in>>Limpeza[i].id_usuario;
		in>>Limpeza[i].id_disciplina;
		in>>Limpeza[i].fotos;
		in>>Externos[i].nome;
		in>>Externos[i].sobrenome;
		in>>Externos[i].id_usuario;
		in>>Externos[i].id_disciplina;
		in>>Externos[i].fotos;
		}
	for (i=0; i<10; i++){					//Loop responsável pela leitura das reservas em disco, e atribuição da matriz de horários.
		for (j=0; j<30; j++){
			for(k=0; k<24; k++){
				for (x=0; x<30; x++){
					in>>Salas[i].horario[j][k].ids[x];
				}			
			}		
		}	
	}		
}

void saida(){							//Função responsável pela inserção dos dados presentes nas variáveis globais no disco.
	int i,j,k,x;
	ofstream out; 
	out.open("BANCO.txt");		//Abertura do arquivo.
	out<<numero_alunos;				//Início da escrita dos dados.
	out<<"\n";
	out<<numero_professores;
	out<<"\n";
	out<<numero_segurancas;
	out<<"\n";
	out<<numero_limpeza;
	out<<"\n";
	out<<numero_externos;
	out<<"\n";
	out<<numero_suporte;
	out<<"\n";
	for(i=0;i<20;i++){							//Loop responsável pela inserção de cada instância de usuário em disco.
		out<<Alunos[i].nome;
		out<<"\n";
		out<<Alunos[i].sobrenome;
		out<<"\n";
		out<<Alunos[i].id_usuario;
		out<<"\n";
		out<<Alunos[i].id_disciplina;
		out<<"\n";
		out<<Alunos[i].fotos;
		out<<"\n";
		out<<Professores[i].nome;
		out<<"\n";
		out<<Professores[i].sobrenome;
		out<<"\n";
		out<<Professores[i].id_usuario;
		out<<"\n";
		out<<Professores[i].id_disciplina;
		out<<"\n";
		out<<Professores[i].fotos;
		out<<"\n";
		out<<Segurancas[i].nome;
		out<<"\n";
		out<<Segurancas[i].sobrenome;
		out<<"\n";
		out<<Segurancas[i].id_usuario;
		out<<"\n";
		out<<Segurancas[i].id_disciplina;
		out<<"\n";
		out<<Segurancas[i].fotos;
		out<<"\n";
		out<<Suporte[i].nome;
		out<<"\n";
		out<<Suporte[i].sobrenome;
		out<<"\n";
		out<<Suporte[i].id_usuario;
		out<<"\n";
		out<<Suporte[i].id_disciplina;
		out<<"\n";
		out<<Suporte[i].fotos;
		out<<"\n";
		out<<Limpeza[i].nome;
		out<<"\n";
		out<<Limpeza[i].sobrenome;
		out<<"\n";
		out<<Limpeza[i].id_usuario;
		out<<"\n";
		out<<Limpeza[i].id_disciplina;
		out<<"\n";
		out<<Limpeza[i].fotos;
		out<<"\n";
		out<<Externos[i].nome;
		out<<"\n";
		out<<Externos[i].sobrenome;
		out<<"\n";
		out<<Externos[i].id_usuario;
		out<<"\n";
		out<<Externos[i].id_disciplina;
		out<<"\n";
		out<<Externos[i].fotos;
		out<<"\n";
	}	
		for (i=0; i<10; i++){							//Loop responsável pela inserção da matriz de horários em disco.
			for (j=0; j<30; j++){
				for(k=0; k<24; k++){
					for (x=0; x<30; x++){
						out<<Salas[i].horario[j][k].ids[x];
						out<<"\n";
					}			
				}		
			}	
		}		
	out.close();
}

void resetar_horarios(){				//Função responsável pela reinicialização da matriz de horários.
	int i,j,k,x;
		for (i=0; i<10; i++){				//Loop responsável pela atribuição dos valores na matriz.
			for (j=0; j<30; j++){
				for(k=0; k<24; k++){
					for (x=0; x<30; x++){
						Salas[i].horario[j][k].ids[x] = 0;
					}			
				}		
			}	
		}
}

void flush_in() {											//Função reponsável pela limpeza do buffer de entrada do teclado.
    int ch;
    while (ch != EOF && ch != '\n') {
        ch = fgetc(stdin);
    } 
}

int menu(){											//Função que printa as opções iniciais do programa, e envia a opção escolhida para a main.
	int opcao;
	cout << "\nMenu:\n\n1-Cadastro\n2-Remocao\n3-Consulta\n4-Resetar banco de dados\n5-Sair\n" << endl; 
	cout << "\nDigite a opcao desejada: "; 
	cin >> opcao;
	flush_in();
	return opcao;
}

void Cadastro(){		//Função responsável pela criação de usuários e reservas.
	
	int opcao=0; int i, j, k, x;
	while (opcao != 8){                    //Loop reponsável pela repetição do menu de cadastro.
		cout << "\nTipo de dado a ser cadastrado:\n\n1-Aluno\n2-Professor\n3-Funcionario de Limpeza\n4-Seguranca\n5-Funcionario de suporte\n6-Externo\n7-Reserva\n8-sair\n" << endl; 
		cout << "\nDigite a opcao desejada: ";
		cin >> opcao;
		flush_in();
		if (opcao==8){	//Condicional responsável pela saída da função, caso o usuário digite tal opção.
			break;
		}
		if (opcao!=7){									//Leitura dos dados de cadastro.
			cout << "\n\nDigite o nome: ";
			getline(cin, P.nome);
			cout << "Digite o sobrenome: ";
			getline(cin, P.sobrenome);
			cout << "Digite o diretorio das fotos: ";
			getline(cin, P.fotos);
			cout << "Digite o id de usuario ou CPF: ";
			cin >> P.id_usuario;
			flush_in();
			if(opcao<3){								//Condicional responsável por possibilitar a criação de id de disciplina para alunos e professores.
				cout << "Digite o id de disciplina: ";
				cin >> P.id_disciplina; 
				flush_in();
			}
			else {
				P.id_disciplina = 0;			
			}
		}
		switch (opcao){
			case 1:{															//Case para a adição dos dados lidos na categoria Aluno.
				Alunos[numero_alunos].nome=P.nome;
				Alunos[numero_alunos].sobrenome=P.sobrenome;
				Alunos[numero_alunos].id_usuario=P.id_usuario;
				Alunos[numero_alunos].id_disciplina=P.id_disciplina;
				Alunos[numero_alunos].fotos=P.fotos;
				numero_alunos++;
				break;			
			}	
			case 2:{
				Professores[numero_professores].nome=P.nome;	//Case para a adição dos dados lidos na categoria Professor.
				Professores[numero_professores].sobrenome=P.sobrenome;
				Professores[numero_professores].id_usuario=P.id_usuario;
				Professores[numero_professores].id_disciplina=P.id_disciplina;
				Professores[numero_professores].fotos=P.fotos;
				numero_professores++;		
				break;	
			}	
			case 3:{
				Limpeza[numero_limpeza].nome=P.nome;		//Case para a adição dos dados lidos na categoria Funcionário de Limpeza.
				Limpeza[numero_limpeza].sobrenome=P.sobrenome;
				Limpeza[numero_limpeza].id_usuario=P.id_usuario;
				Limpeza[numero_limpeza].id_disciplina=0;
				Limpeza[numero_limpeza].fotos=P.fotos;
				numero_limpeza++;			
				break;		
			}
			case 4:{
				Segurancas[numero_segurancas].nome=P.nome;	//Case para a adição dos dados lidos na categoria Seguranças.
				Segurancas[numero_segurancas].sobrenome=P.sobrenome;
				Segurancas[numero_segurancas].id_usuario=P.id_usuario;
				Segurancas[numero_segurancas].id_disciplina=0;
				Segurancas[numero_segurancas].fotos=P.fotos;
				numero_segurancas++;	
				break;			
			}
			case 5:{
				Suporte[numero_suporte].nome=P.nome;		//Case para a adição dos dados lidos na categoria Suporte.
				Suporte[numero_suporte].sobrenome=P.sobrenome;
				Suporte[numero_suporte].id_usuario=P.id_usuario;
				Suporte[numero_suporte].id_disciplina=0;
				Suporte[numero_suporte].fotos=P.fotos;
				numero_suporte++;
				break;				
			}
			case 6:{																				//Case para a adição dos dados lidos na categoria de usuários externos.
				Externos[numero_externos].nome=P.nome;
				Externos[numero_externos].sobrenome=P.sobrenome;
				Externos[numero_externos].id_usuario=P.id_usuario;
				Externos[numero_externos].id_disciplina=0;
				Externos[numero_externos].fotos=P.fotos;
				numero_externos++;	
				break;	
			}
			case 7:{													//Case para a criação de uma reserva.
				Ids vazio;
				for (i=0; i<30; i++){
					vazio.ids[i]=0;	
				}
				cout << "\nDigite o numero da sala da reserva: ";	//Leitura de dados para criação da reserva.
				cin >> i;
				flush_in();	
				cout << "\nDigite o dia da reserva: ";
				cin >> j;
				flush_in();	
				cout << "\nDigite o horario da reserva: ";
				cin >> k;
				flush_in();
				if (Salas[i-1].horario[j-1][k].ids[0] != vazio.ids[0]){	//Condicional para verificar se o horário solicitado já está reservado.
					cout << "\n\nErro, horário ja reservado." << endl;
					break;
				}
				cout << "\nDigite o id do responsavel pela reserva: ";  //Continuação da leitura de dados.
				cin >> P.id_usuario;
				flush_in();
				cout << "\nDigite o id da disciplina (ou -1 caso a reserva nao seja destinada para aula): ";
				cin >> P.id_disciplina;
				flush_in();
				vazio.ids[0]=P.id_disciplina;
				vazio.ids[1]=P.id_usuario;
				for (x=2;((x<30)&&(vazio.ids[x-1]!=-1)); x++){
					cout << "\nDigite o id do participante da atividade (ou -1 caso não existam mais participantes): ";
					cin >> vazio.ids[x];
					flush_in();
				}
				Salas[i-1].horario[j-1][k] = vazio;  //Inserção dos dados lidos na matriz de horários.
				break;	
			}
		}
	}	
}

void remocao(){				//Função responsável pela remoção de dados cadastrados.

	int id, opcao,i,j,k;

	opcao=0;
	while (opcao!=8){  //Loop reponsável por manter o usuário no menu de remoção, até que solicite a saída.
		cout << "\nTipo de dado a ser removido:\n\n1-Aluno\n2-Professor\n3-Funcionario de Limpeza\n4-Seguranca\n5-Funcionario de suporte\n6-Externo\n7-Reserva\n8-Sair\n" << endl; 
		cout << "\nDigite a opcao desejada: ";
		cin >> opcao;
		flush_in();
		if (opcao==8){
			break;
		}
		if (opcao!=7){		//Leitura do id de usuário à ser removido.
			cout << "\nDigite o id do usuario a ser removido: ";
			cin >> id;
			flush_in();
		}
		switch (opcao){		//Case para a remoção dos diferentes tipos de usuários.
			case 1:{
				i=0;
				while ((Alunos[i].id_usuario != id)&&(i<=(numero_alunos-1))){ //Loop que verifica em que posição do vetor de alunos o id se encontra.
					i++;
				}																		//Case para a remoção de alunos.
				if (i>(numero_alunos-1)){						//Condicional para verificação se o id informado existe no banco de dados.
					cout << "\nId não encontrado\n";
					break;
				}
				if (i==(numero_alunos-1)){		//Condicional que verifica se o id informado está no final do vetor de alunos,
					Alunos[i].nome="";					// e removendo o último aluno.
					Alunos[i].sobrenome="";
					Alunos[i].id_usuario=0;
					Alunos[i].id_disciplina=0;
					Alunos[i].fotos="";
					numero_alunos--;	
					break;		
				}	
				if (i<(numero_alunos-1)){													//Condicional que verifica se o id informado não está no final do vetor de alunos,
					Alunos[i].nome = Alunos[(numero_alunos-1)].nome;		// caso não esteja no final, o usuário escolhido será removido, e o último
					Alunos[i].sobrenome = Alunos[(numero_alunos-1)].sobrenome;	// membro entrará na posição do usuário excluído.
					Alunos[i].id_usuario = Alunos[(numero_alunos-1)].id_usuario;
					Alunos[i].id_disciplina = Alunos[(numero_alunos-1)].id_disciplina;
					Alunos[i].fotos = Alunos[(numero_alunos-1)].fotos;
					Alunos[(numero_alunos-1)].nome="";
					Alunos[(numero_alunos-1)].sobrenome="";
					Alunos[(numero_alunos-1)].id_usuario=0;
					Alunos[(numero_alunos-1)].id_disciplina=0;
					Alunos[(numero_alunos-1)].fotos="";
					numero_alunos--;	
					break;
				}	
			}	
			case 2:{			//Case com as mesmas funções do case de alunos.
				i=0;
				while ((Professores[i].id_usuario != id)&&(i<=(numero_professores-1))){
					i++;
				}
				if (i>(numero_professores-1)){
					cout << "\nId não encontrado\n";
					break;
				}
				if (i==(numero_professores-1)){
					Professores[i].nome="";
					Professores[i].sobrenome="";
					Professores[i].id_usuario=0;
					Professores[i].id_disciplina=0;
					Professores[i].fotos="";
					numero_professores--;	
					break;		
				}	
				if (i<(numero_professores-1)){
					Professores[i].nome = Professores[(numero_professores-1)].nome;
					Professores[i].sobrenome = Professores[(numero_professores-1)].sobrenome;
					Professores[i].id_usuario = Professores[(numero_professores-1)].id_usuario;
					Professores[i].id_disciplina = Professores[(numero_professores-1)].id_disciplina;
					Professores[i].fotos = Professores[(numero_professores-1)].fotos;
					Professores[(numero_professores-1)].nome="";
					Professores[(numero_professores-1)].sobrenome="";
					Professores[(numero_professores-1)].id_usuario=0;
					Professores[(numero_professores-1)].id_disciplina=0;
					Professores[(numero_professores-1)].fotos="";
					numero_professores--;	
					break;
				}		
			}	
			case 3:{			//Case com as mesmas funções do case de alunos.
				i=0;
				while ((Limpeza[i].id_usuario != id)&&(i<=(numero_limpeza-1))){
					i++;
				}
				if (i>(numero_limpeza-1)){
					cout << "\nId não encontrado\n";
					break;
				}
				if (i==(numero_limpeza-1)){
					Limpeza[i].nome="";
					Limpeza[i].sobrenome="";
					Limpeza[i].id_usuario=0;
					Limpeza[i].id_disciplina=0;
					Limpeza[i].fotos="";
					numero_limpeza--;	
					break;		
				}	
				if (i<(numero_limpeza-1)){
					Limpeza[i].nome = Limpeza[(numero_limpeza-1)].nome;
					Limpeza[i].sobrenome = Limpeza[(numero_limpeza-1)].sobrenome;
					Limpeza[i].id_usuario = Limpeza[(numero_limpeza-1)].id_usuario;
					Limpeza[i].id_disciplina = Limpeza[(numero_limpeza-1)].id_disciplina;
					Limpeza[i].fotos = Limpeza[(numero_limpeza-1)].fotos;
					Limpeza[(numero_limpeza-1)].nome="";
					Limpeza[(numero_limpeza-1)].sobrenome="";
					Limpeza[(numero_limpeza-1)].id_usuario=0;
					Limpeza[(numero_limpeza-1)].id_disciplina=0;
					Limpeza[(numero_limpeza-1)].fotos="";
					numero_limpeza--;	
					break;
				}				
			}
			case 4:{		//Case com as mesmas funções do case de alunos.
				i=0;
				while ((Segurancas[i].id_usuario != id)&&(i<=(numero_segurancas-1))){
					i++;
				}
				if (i>(numero_segurancas-1)){
					cout << "\nId não encontrado\n";
					break;
				}
				if (i==(numero_segurancas-1)){
					Segurancas[i].nome="";
					Segurancas[i].sobrenome="";
					Segurancas[i].id_usuario=0;
					Segurancas[i].id_disciplina=0;
					Segurancas[i].fotos="";
					numero_segurancas--;	
					break;		
				}	
				if (i<(numero_segurancas-1)){
					Segurancas[i].nome = Segurancas[(numero_segurancas-1)].nome;
					Segurancas[i].sobrenome = Segurancas[(numero_segurancas-1)].sobrenome;
					Segurancas[i].id_usuario = Segurancas[(numero_segurancas-1)].id_usuario;
					Segurancas[i].id_disciplina = Segurancas[(numero_segurancas-1)].id_disciplina;
					Segurancas[i].fotos = Segurancas[(numero_segurancas-1)].fotos;
					Segurancas[(numero_segurancas-1)].nome="";
					Segurancas[(numero_segurancas-1)].sobrenome="";
					Segurancas[(numero_segurancas-1)].id_usuario=0;
					Segurancas[(numero_segurancas-1)].id_disciplina=0;
					Segurancas[(numero_segurancas-1)].fotos="";
					numero_segurancas--;	
					break;
				}						
			}
			case 5:{	//Case com as mesmas funções do case de alunos.
				i=0;
				while ((Suporte[i].id_usuario != id)&&(i<=(numero_suporte-1))){
					i++;
				}
				if (i>(numero_suporte-1)){
					cout << "\nId não encontrado\n";
					break;
				}
				if (i==(numero_suporte-1)){
					Suporte[i].nome="";
					Suporte[i].sobrenome="";
					Suporte[i].id_usuario=0;
					Suporte[i].id_disciplina=0;
					Suporte[i].fotos="";
					numero_suporte--;	
					break;		
				}	
				if (i<(numero_suporte-1)){
					Suporte[i].nome = Suporte[(numero_suporte-1)].nome;
					Suporte[i].sobrenome = Suporte[(numero_suporte-1)].sobrenome;
					Suporte[i].id_usuario = Suporte[(numero_suporte-1)].id_usuario;
					Suporte[i].id_disciplina = Suporte[(numero_suporte-1)].id_disciplina;
					Suporte[i].fotos = Suporte[(numero_suporte-1)].fotos;
					Suporte[(numero_suporte-1)].nome="";
					Suporte[(numero_suporte-1)].sobrenome="";
					Suporte[(numero_suporte-1)].id_usuario=0;
					Suporte[(numero_suporte-1)].id_disciplina=0;
					Suporte[(numero_suporte-1)].fotos="";
					numero_suporte--;	
					break;
				}						
			}
			case 6:{	//Case com as mesmas funções do case de alunos.
				i=0;
				while ((Externos[i].id_usuario != id)&&(i<=(numero_externos-1))){
					i++;
				}
				if (i>(numero_externos-1)){
					cout << "\nId não encontrado\n";
					break;
				}
				if (i==(numero_externos-1)){
					Externos[i].nome="";
					Externos[i].sobrenome="";
					Externos[i].id_usuario=0;
					Externos[i].id_disciplina=0;
					Externos[i].fotos="";
					numero_externos--;	
					break;		
				}	
				if (i<(numero_externos-1)){
					Externos[i].nome = Suporte[(numero_externos-1)].nome;
					Externos[i].sobrenome = Suporte[(numero_externos-1)].sobrenome;
					Externos[i].id_usuario = Suporte[(numero_externos-1)].id_usuario;
					Externos[i].id_disciplina = Suporte[(numero_externos-1)].id_disciplina;
					Externos[i].fotos = Suporte[(numero_externos-1)].fotos;
					Externos[(numero_externos-1)].nome="";
					Externos[(numero_externos-1)].sobrenome="";
					Externos[(numero_externos-1)].id_usuario=0;
					Externos[(numero_externos-1)].id_disciplina=0;
					Externos[(numero_externos-1)].fotos="";
					numero_externos--;	
					break;
				}		
			}
			case 7:{				//Case para a remoção de reserva.
				Ids vazio;
				for (i=0; i<30; i++){			//Loop que zera o vetor auxiliar.
					vazio.ids[i]=0;	
				}
				cout << "\nDigite o numero da sala em que a reserva sera removida: ";	//Solicitação de dados para efetuar a remoção.
				cin >> i;
				flush_in();	
				cout << "\nDigite o dia da reserva que sera removida: ";
				cin >> j;
				flush_in();	
				cout << "\nDigite o horario da reserva que sera removida: ";
				cin >> k;
				flush_in();
				Salas[i-1].horario[j-1][k] = vazio;	//Remoção do horário solicitado.
				break;			
			}
		break;
		}	
	}
} 

void consulta(){		//Função responsável pela consulta de dados no banco.
	
	int i,j,k,opcao,id;

	opcao=0;
	while (opcao!=8){		//Loop para manter o usuário no menu de consulta até que o mesmo solicite a saída.
		cout << "\nTipo de dado a ser consultado:\n\n1-Aluno\n2-Professor\n3-Funcionario de Limpeza\n4-Seguranca\n5-Funcionario de suporte\n6-Externo\n7-Horário\n8-Sair\n" << endl; 
		cout << "\nDigite a opcao desejada: ";
		cin >> opcao;
		flush_in();
		if (opcao==8){
			break;
		}
		switch (opcao){
			case 1:{			//Case para a consulta de alunos.
				cout << "\nDigite o id do aluno: ";		//Leitura do id de alunos.
				cin >> id;
				flush_in();
				i=0;
				while ((Alunos[i].id_usuario != id)&&(i<=(numero_alunos-1))){ //Loop responsável pela busca do id informado no vetor de alunos.
					i++;
				}
				if (i>(numero_alunos-1)){			//Condicional para a verificação se o id informado está presente no banco.
					cout << "\nId não encontrado\n";
					break;
				}
				cout << "\nNome: " << Alunos[i].nome << endl;	//Print dos dados solicitados.
				cout << "Sobrenome: " << Alunos[i].sobrenome << endl;
				cout << "Id de usuario: " << Alunos[i].id_usuario << endl;
				cout << "Id de disciplina: " << Alunos[i].id_disciplina << endl;
				cout << "Diretorio de fotos: " << Alunos[i].fotos << endl;
				break;				
			}
			case 2:{  //Case semelhante ao de consulta de alunos.
				cout << "\nDigite o id do professor: ";
				cin >> id;
				flush_in();
				i=0;
				while ((Professores[i].id_usuario != id)&&(i<=(numero_professores-1))){
					i++;
				}
				if (i>(numero_professores-1)){
					cout << "\nId não encontrado\n";
					break;
				}
				cout << "\nNome: " << Professores[i].nome << endl;
				cout << "Sobrenome: " << Professores[i].sobrenome << endl;
				cout << "Id de usuario: " << Professores[i].id_usuario << endl;
				cout << "Id de disciplina: " << Professores[i].id_disciplina << endl;
				cout << "Diretorio de fotos: " << Professores[i].fotos << endl;
				break;				
			}
			case 3:{	//Case semelhante ao de consulta de alunos.
				cout << "\nDigite o id do funcionario de limpeza: ";
				cin >> id;
				flush_in();
				i=0;
				while ((Limpeza[i].id_usuario != id)&&(i<=(numero_limpeza-1))){
					i++;
				}
				if (i>(numero_limpeza-1)){
					cout << "\nId não encontrado\n";
					break;
				}
				cout << "\nNome: " << Limpeza[i].nome << endl;
				cout << "Sobrenome: " << Limpeza[i].sobrenome << endl;
				cout << "Id de usuario: " << Limpeza[i].id_usuario << endl;
				cout << "Diretorio de fotos: " << Limpeza[i].fotos << endl;
				break;
			}
			case 4:{	//Case semelhante ao de consulta de alunos.
				cout << "\nDigite o id do seguranca: ";
				cin >> id;
				flush_in();
				i=0;
				while ((Segurancas[i].id_usuario != id)&&(i<=(numero_segurancas-1))){
					i++;
				}
				if (i>(numero_segurancas-1)){
					cout << "\nId não encontrado\n";
					break;
				}
				cout << "\nNome: " << Segurancas[i].nome << endl;
				cout << "Sobrenome: " << Segurancas[i].sobrenome << endl;
				cout << "Id de usuario: " << Segurancas[i].id_usuario << endl;
				cout << "Diretorio de fotos: " << Segurancas[i].fotos << endl;
				break;
			}
			case 5:{	//Case semelhante ao de consulta de alunos.
				cout << "\nDigite o id do funcionario de suporte: ";
				cin >> id;
				flush_in();
				i=0;
				while ((Suporte[i].id_usuario != id)&&(i<=(numero_suporte-1))){
					i++;
				}
				if (i>(numero_suporte-1)){
					cout << "\nId não encontrado\n";
					break;
				}
				cout << "Nome: " << Suporte[i].nome << endl;
				cout << "Sobrenome: " << Suporte[i].sobrenome << endl;
				cout << "Id de usuario: " << Suporte[i].id_usuario << endl;
				cout << "Diretorio de fotos: " << Suporte[i].fotos << endl;
				break;
			}
			case 6:{	//Case semelhante ao de consulta de alunos.
				cout << "\nDigite o id do usuario externo: ";
				cin >> id;
				flush_in();
				i=0;
				while ((Externos[i].id_usuario != id)&&(i<=(numero_externos-1))){
					i++;
				}
				if (i>(numero_externos-1)){
					cout << "\nId não encontrado\n";
					break;
				}
				cout << "\nNome: " << Externos[i].nome << endl;
				cout << "Sobrenome: " << Externos[i].sobrenome << endl;
				cout << "Id de usuario: " << Externos[i].id_usuario << endl;
				cout << "Diretorio de fotos: " << Externos[i].fotos << endl;
				break;
			}
			case 7:{	//Case semelhante ao de consulta de alunos.
				int x;
				cout << "\nDigite o numero da sala da reserva: ";
				cin >> i;
				flush_in();	
				cout << "\nDigite o dia da reserva: ";
				cin >> j;
				flush_in();	
				cout << "\nDigite o horario da reserva: ";
				cin >> k;
				flush_in();
				if (Salas[i-1].horario[j-1][k].ids[0] == 0){
					cout << "\nO horario do da sala: " << i ;
					cout << "  Do dia: " << j;
					cout << "  E do horario de : " << k;
					cout << "h, está vazio." << endl;
					break;
				}
				cout << "\nId de usuario resposável pela reserva: " << Salas[i-1].horario[j-1][k].ids[0] << endl;
				cout << "\nId de disciplina: " << Salas[i-1].horario[j-1][k].ids[1] << endl;
				for (x=2;((x<30)&&(Salas[i-1].horario[j-1][k].ids[x]!=-1)); x++){
					cout << "\nId do participante da atividade: " << Salas[i-1].horario[j-1][k].ids[x] << endl;;
				}
				break;
			}
		}	
	}
}
void resetar_banco(){ //Função responsável pela reinicialização completa do banco de dados.
	int i;
	resetar_horarios();				//Chamada da função que reseta a matriz de horários.
	for(i=0;i<20;i++){				//Loop responsável por zerar os dados de cada instância de usuário.
		Alunos[i].nome="0";					
		Alunos[i].sobrenome="0";
		Alunos[i].id_usuario=0;
		Alunos[i].id_disciplina=0;		
		Alunos[i].fotos="0";
		Professores[i].nome="0";
		Professores[i].sobrenome="0";
		Professores[i].id_usuario=0;
		Professores[i].id_disciplina=0;
		Professores[i].fotos="0";
		Segurancas[i].nome="0";
		Segurancas[i].sobrenome="0";
		Segurancas[i].id_usuario=0;
		Segurancas[i].id_disciplina=0;
		Segurancas[i].fotos="0";
		Suporte[i].nome="0";
		Suporte[i].sobrenome="0";
		Suporte[i].id_usuario=0;
		Suporte[i].id_disciplina=0;
		Suporte[i].fotos="0";
		Limpeza[i].nome="0";
		Limpeza[i].sobrenome="0";
		Limpeza[i].id_usuario=0;
		Limpeza[i].id_disciplina=0;
		Limpeza[i].fotos="0";
		Externos[i].nome="0";
		Externos[i].sobrenome="0";
		Externos[i].id_usuario=0;
		Externos[i].id_disciplina=0;
		Externos[i].fotos="0";
		}
		numero_alunos=0;
		numero_professores=0;
		numero_segurancas=0;
		numero_limpeza=0;
		numero_externos=0;
		numero_suporte=0;
		saida();
		
}
int main(){			//Função main.
 	
	int i=0;
	int opcao, resultado;
	string nome, sobrenome, fotos;
	long id_usuario, id_disciplina;
	
	
	opcao=0;
	entrada();			//Chamada da função que recupera os dados do banco.
	while (opcao!=5){
		opcao=menu();  //Chamada da função de menu inicial.

		switch (opcao){		//Switch que abre a função determinada pelo usuário.
			case 1:{
				Cadastro();  //Chamada da função de cadastro.
				break;
			}
			case 2:{
				remocao ();		//Chamada da função de remoção.
				break;
			}	
			case 3:{
				consulta ();	//Chamada da função de consulta.
				break;
			}	
			case 4:{
				resetar_banco(); //Chamada da função de reset do banco de dados.
				break;
			}
			case 5:{
				saida();				//Chamada da função de gravação de dados no banco, após a solicitação do usuário de encerrar o programa.
			}
		}
	}
  return 0;
}
