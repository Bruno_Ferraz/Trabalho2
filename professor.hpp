#ifndef _PROFESSOR_HPP
#define _PROFESSOR_HPP

#include "pessoa.hpp"
#include <string>

using namespace std;

class Professor: public Pessoa{

public:
	Professor(){};
	Professor(string nome, string sobrenome, long id_usuario, long id_disciplina, string fotos);
	void setPessoa(Pessoa P);
};

#endif
